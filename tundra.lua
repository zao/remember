local win_common = {
	Env = {
		CPPDEFS = { "_WIN32_WINNT=0x601", "_SCL_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_DEPRECATE", "_CRT_SECURE_NO_DEPRECATE", "UNICODE=1", "_UNICODE=1" },
		CPPDEFS_DEBUG = { "DEBUG", "_DEBUG" },
		CCOPTS = { "/EHsc", "/Z7" },
		CCOPTS_DEBUG      = { "/MDd" },
		CCOPTS_RELEASE    = { "/MD" },
		CCOPTS_PRODUCTION = { "/MD" },
		CXXOPTS = { "/EHsc", "/Z7" },
		CXXOPTS_DEBUG      = { "/MDd" },
		CXXOPTS_RELEASE    = { "/MD" },
		CXXOPTS_PRODUCTION = { "/MD" },
		PROGOPTS  = { "/DEBUG", "/INCREMENTAL:NO" },
		SHLIBOPTS = { "/DEBUG", "/INCREMENTAL:NO" },
		CPPPATH = {
--			"\"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.0A/Include\"",
			"D:/Bounce/tbb40_233oss/include",
			"D:/Local/include",
			"dep/gl3w/include",
		},
	}
}

Build {
	Units = "units.lua",
	Configs = {
		Config {
			Inherit = win_common,
			Name = "win64-vc10",
			DefaultOnHost = "windows",
			Tools = { { "msvc-vs2010"; TargetArch = "x64" } },
			Env = {
				PROGOPTS  = { "/MACHINE:X64", },
				SHLIBOPTS = { "/MACHINE:X64", },
				LIBPATH = {
--					"\"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.0A/Lib/x64\"",
					"D:/Bounce/tbb40_233oss/lib/intel64/vc10",
					"D:/Local/lib64",
				},
			},
		},
		Config {
			Inherit = win_common,
			Name = "win32-vc10",
			DefaultOnHost = "windows",
			Tools = { { "msvc-vs2010"; TargetArch = "x86" } },
			Env = {
				PROGOPTS  = { "/MACHINE:X86", },
				SHLIBOPTS = { "/MACHINE:X86", },
				LIBPATH = {
--					"\"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.0A/Lib\"",
					"D:/Bounce/tbb40_233oss/lib/ia32/vc10",
					"D:/Local/lib32",
				},
			},
		},
	},
}

-- vim: ts=4 sw=4 noet cin
