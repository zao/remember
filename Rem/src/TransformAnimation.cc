#include "TransformAnimation.h"

BounceFlipAnimation::BounceFlipAnimation(float duration, float distance, glm::vec3 alongAxis, float rotations, glm::vec3 aroundAxis, float angleOffset)
	: duration(duration)
	, distance(distance)
	, alongAxis(glm::normalize(alongAxis))
	, rotations(rotations)
	, aroundAxis(glm::normalize(aroundAxis))
	, angleOffset(angleOffset)
{}

glm::mat4 BounceFlipAnimation::Compute(double t) const
{
	float a = t / duration;
	a = std::min(a, 1.0f);
	glm::mat4 tileRotation = glm::rotate(angleOffset + 360*a*rotations, aroundAxis);
	glm::mat4 tileDisplacement = glm::translate(glm::vec3(0.0f, distance*fabs(sin(a*3.14f)), 0.0f));
	return tileDisplacement * tileRotation;
}