#include "Shader.h"
#include <vector>
#include <boost/foreach.hpp>
#include <fstream>
#include "util/DebugHelp.h"
#include "util/Resources.h"

char const* ShaderTypeString(GLenum type)
{
	if (type == GL_VERTEX_SHADER) return "vertex";
	if (type == GL_FRAGMENT_SHADER) return "fragment";
	if (type == GL_GEOMETRY_SHADER) return "geometry";
	return "unknown-type";
}

#include <boost/algorithm/string/replace.hpp>

AnnotatedShader ShaderFromResource(GLenum type, std::string name)
{
	std::vector<char> source;
	{
		std::string s = "res/";
		s += name;
		std::ifstream is(s.c_str(), std::ios::binary);
		if (is)
		{
			is.seekg(0, std::ios::end);
			source.resize(is.tellg());
			if (source.size() > 0)
			{
				is.seekg(0, std::ios::beg);
				is.read(&source[0], source.size());
			}
		}
	}

	if (source.empty())
	{
		std::string s = name;
		BOOST_FOREACH(char& c, s)
		{
			if (c == '.') c = '_';
		}
	
		source = LoadEmbeddedResource(s);
	}

	char const* p = source.empty() ? "" : &source[0];
	GLint cb = source.size();

	GLuint id = glCreateShader(type);
	glShaderSource(id, 1, &p, &cb);
	glCompileShader(id);

	GLint status;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint logLength;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLength);

		std::vector<GLchar> logStr(logLength + 1);
		glGetShaderInfoLog(id, logLength, nullptr, &logStr[0]);
		std::string shaderType = ShaderTypeString(type);

		std::string message = "Compile failure in " + shaderType + " shader: \n"
			+ &logStr[0] + "\n";
		OutputDebugStringA(message.c_str());
		assert("Shader compile failed");
	}

	AnnotatedShader ret = { id };
	return ret;
}
