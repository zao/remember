#include "Mesh.h"
#include "util/Numeric.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/foreach.hpp>

#include <fstream>
#include <iostream>
#include <sstream>

namespace geom
{
	template <typename Vector>
	unsigned ResolveIndex(Vector const& v, int index)
	{
		if (index > 0)
			return (unsigned)index;
		return v.size() + index;
	}

	boost::shared_ptr<AbstractMesh> LoadObj(std::string filename)
	{
		boost::shared_ptr<AbstractMesh> ret = boost::make_shared<AbstractMesh>();
		
		struct Triad
		{
			unsigned position;
			unsigned normal;
			unsigned texcoord;

			bool operator < (Triad const& rhs) const
			{
				if (position != rhs.position) return position < rhs.position;
				if (normal != rhs.normal) return normal < rhs.normal;
				return texcoord < rhs.texcoord;
			}
		};

		struct Vertex
		{
			glm::vec3 position;
			glm::vec3 normal;
			glm::vec2 texcoord;
		};

		std::vector<glm::vec3> positions(1);
		std::vector<glm::vec3> normals(1);
		std::vector<glm::vec2> texcoords(1);

		typedef uint32_t Index;
		std::map<Triad, unsigned> vertexMap;
		std::vector<Vertex> vertices;
		std::vector<Index> indices;

		std::vector<Object> objects;

		std::ifstream is(filename.c_str());
		if (!is)
			return boost::shared_ptr<AbstractMesh>();

		std::string line;
		while (std::getline(is, line))
		{
			if (line.size() < 3 || line[0] == '#')
				continue;
			std::istringstream lineStream(line);
			std::string firstWord;
			lineStream >> firstWord;
			if (firstWord == "v")
			{
				glm::vec3 pos;
				lineStream >> pos.x >> pos.y >> pos.z;
				positions.push_back(pos);
			}
			else if (firstWord == "vn")
			{
				glm::vec3 n;
				lineStream >> n.x >> n.y >> n.z;
				normals.push_back(n);
			}
			else if (firstWord == "vt")
			{
				glm::vec2 tc;
				lineStream >> tc.x >> tc.y;
				tc.y = 1.0f - tc.y;
				texcoords.push_back(tc);
			}
			else if (firstWord == "f")
			{
				for (int i = 0; i < 3; ++i)
				{
					Triad triad = {};
					std::string tup;
					lineStream >> tup;
					std::vector<std::string> parts;
					boost::algorithm::split(parts, tup, boost::algorithm::is_any_of("/"));
					if (parts.size() >= 1)
						triad.position = ResolveIndex(positions, atoi(parts[0].c_str()));
					if (parts.size() >= 2)
						triad.texcoord = ResolveIndex(texcoords, atoi(parts[1].c_str()));
					if (parts.size() >= 3)
						triad.normal = ResolveIndex(normals, atoi(parts[2].c_str()));
					auto I = vertexMap.find(triad);
					if (I != vertexMap.end())
					{
						indices.push_back(I->second);
					}
					else
					{
						Vertex v = {
							positions[triad.position],
							normals[triad.normal],
							texcoords[triad.texcoord]
						};
						vertexMap[triad] = vertices.size();
						indices.push_back(vertices.size());
						vertices.push_back(v);
					}
				}
				objects.back().primitiveCount++;
			}
			else if (firstWord == "g" || firstWord == "usemtl")
			{
				if (objects.empty() || objects.back().primitiveCount > 0)
				{
					Object obj = {};
					obj.offset = indices.size();
					objects.push_back(obj);
				}
				if (firstWord == "g")
				{
					lineStream >> objects.back().name;
				}
				else if (firstWord == "usemtl")
				{
					lineStream >> objects.back().material;
				}
			}
		}

		AbstractBinding bindings[] = {
			MakeAbstractBinding("POSITION", 0, 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0),
			MakeAbstractBinding("NORMAL",   0, 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), sizeof(glm::vec3)),
			MakeAbstractBinding("TEXCOORD", 0, 0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 2*sizeof(glm::vec3))
		};
		ret->bindings.insert(bindings, bindings + 3);
		ret->indexBuffer = MakeRawBuffer(indices);
		ret->vertexBuffers.push_back(MakeRawBuffer(vertices));
		ret->objects = objects;

		return ret;
	}
}