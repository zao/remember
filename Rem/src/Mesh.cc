#include "Mesh.h"
#include "util/Numeric.h"

#include <boost/foreach.hpp>

namespace geom
{
	bool TestLineAABB(glm::vec3 p0, glm::vec3 p1, AABB const& b)
	{
		glm::vec3 c = (b.min + b.max) * 0.5f;
		glm::vec3 e = b.max - c;
		glm::vec3 m = (p0 + p1) * 0.5f;
		glm::vec3 d = p1 - m;
		m = m - c;

		float adx = fabs(d.x);
		if (fabs(m.x) > e.x + adx) return false;
		float ady = fabs(d.y);
		if (fabs(m.y) > e.y + ady) return false;
		float adz = fabs(d.z);
		if (fabs(m.z) > e.z + adz) return false;

		static float const Epsilon = 0.00001f; // huhu
		adx += Epsilon; ady += Epsilon; adz += Epsilon;

		if (fabs(m.y * d.z - m.z * d.y) > e.y * adz + e.z * ady) return false;
		if (fabs(m.z * d.x - m.x * d.z) > e.x * adz + e.z * adx) return false;
		if (fabs(m.x * d.y - m.y * d.x) > e.x * ady + e.y * adx) return false;

		return true;
	}

	bool TestLineOBB(glm::vec3 p0, glm::vec3 p1, OBB const& b)
	{
		glm::vec3 localP0 = (p0 - b.pos) * b.basis;
		glm::vec3 localP1 = (p1 - b.pos) * b.basis;
		AABB aabb = { -b.half, b.half };
		return TestLineAABB(localP0, localP1, aabb);
	}

	LineFindResult LineFindResult::Reject()
	{
		LineFindResult ret = { false };
		return ret;
	}

	LineFindResult LineFindResult::Intersect(float t)
	{
		LineFindResult ret = { true, t };
		return ret;
	}

	LineFindResult FindLineAABB(glm::vec3 p0, glm::vec3 p1, AABB const& b)
	{
		return LineFindResult::Reject();
	}

	LineFindResult FindLineOBB(glm::vec3 p0, glm::vec3 p1, OBB const& b)
	{
		static float const Epsilon = 0.0000000001f;
		static float const Infinity = 100000.0f;
		float tMin = -Infinity, tMax = Infinity;
		glm::vec3 p = b.pos - p0;
		glm::vec3 d = glm::normalize(p1 - p0);
		for (int i = 0; i < 3; ++i)
		{
			float e = glm::dot(b.basis[i], p);
			float f = glm::dot(b.basis[i], d);
			if (fabs(f) > Epsilon)
			{
				float t1 = (e + b.half[i])/f;
				float t2 = (e - b.half[i])/f;
				if (t1 > t2) std::swap(t1, t2);
				tMin = std::max(t1, tMin);
				tMax = std::min(t2, tMax);
				if (tMin > tMax || tMax < 0) return LineFindResult::Reject();
			}
			else if (-e - b.half[i] > 0 || -e + b.half[i] < 0) return LineFindResult::Reject();
		}
		if (tMin > 0) return LineFindResult::Intersect(tMin);
		else return LineFindResult::Intersect(tMax);
	}

	struct FindLineVisitor : boost::static_visitor<LineFindResult>
	{
		FindLineVisitor(glm::vec3 p0, glm::vec3 p1)
			: p0(p0), p1(p1)
		{}

		LineFindResult operator () (AABB const& aabb) const
		{
			return FindLineAABB(p0, p1, aabb);
		}

		LineFindResult operator () (OBB const& obb) const
		{
			return FindLineOBB(p0, p1, obb);
		}

		glm::vec3 p0, p1;
	};

	LineFindResult FindLine(glm::vec3 p0, glm::vec3 p1, BoundingVolume const& bv)
	{
		return boost::apply_visitor(FindLineVisitor(p0, p1), bv);
	}

	struct TransformVisitor : boost::static_visitor<BoundingVolume>
	{
		explicit TransformVisitor(glm::mat4 xform)
			: xform(xform)
		{}

		OBB operator () (AABB const& aabb) const
		{
			OBB obb;
			obb.basis = glm::mat3();
			obb.pos = (aabb.min + aabb.max) / 2.0f;
			obb.half = (aabb.max - aabb.min) / 2.0f;
			return operator () (obb);
		}

		OBB operator () (OBB obb) const
		{
			obb.basis = (glm::mat3)xform * obb.basis;
			glm::vec4 newPos = xform * glm::vec4(obb.pos, 1.0f);
			obb.pos = glm::vec3(newPos) / newPos.w;
			return obb;
		}

		glm::mat4 xform;
	};

	BoundingVolume TransformBoundingVolume(glm::mat4 const& xform, BoundingVolume const& bv)
	{
		return boost::apply_visitor(TransformVisitor(xform), bv);
	}

	AbstractBinding MakeAbstractBinding(
		std::string semanticName,
		unsigned semanticOrdinal,
		unsigned bufferIndex,
		unsigned componentCount,
		GLenum componentType,
		GLboolean shouldNormalize,
		GLsizei attributeStride,
		unsigned byteOffset)
	{
		AbstractBinding ret;
		CommonBinding common = {
			bufferIndex, componentCount, componentType,
			shouldNormalize, attributeStride, byteOffset };
		(CommonBinding&)ret = common;
		ret.semanticName = semanticName;
		ret.semanticOrdinal = semanticOrdinal;
		return ret;
	}

	boost::shared_ptr<ResolvedMesh> ResolveMesh(AbstractMesh const& source, ShaderLocations const& locs)
	{
		boost::shared_ptr<ResolvedMesh> ret(new ResolvedMesh);
		
		size_t numVBs = source.vertexBuffers.size();
		std::vector<GLuint> rawVBs(numVBs);

		assert(numVBs);
		glGenBuffers(numVBs, &rawVBs[0]);

		for (size_t i = 0; i < numVBs; ++i)
		{
			auto vb = rawVBs[i];
			auto const& src = source.vertexBuffers[i];
			glBindBuffer(GL_ARRAY_BUFFER, vb);
			glBufferData(GL_ARRAY_BUFFER, src.size, src.data.get(), GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			ret->vbs.push_back(boost::shared_ptr<GLuint>(new GLuint(vb), &DeleteGLBuffer));
		}

		GLuint ib;
		glGenBuffers(1, &ib);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, source.indexBuffer.size, source.indexBuffer.data.get(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		ret->ib.reset(new GLuint(ib), &DeleteGLBuffer);

		BOOST_FOREACH(auto const& binding, source.bindings)
		{
			ResolvedBinding b;
			(CommonBinding&)b = binding;
			auto sem = MakeSemantic(binding.semanticName, binding.semanticOrdinal);
			auto I = locs.mapping.find(sem);
			if (I == locs.mapping.end())
				continue;
			b.attributeIndex = I->second;
			ret->bindings.insert(b);

			if (binding.semanticName == "POSITION")
			{
				auto posBuffer = source.vertexBuffers[binding.bufferIndex];
				auto fp = posBuffer.data.get() + binding.byteOffset;
				int n = posBuffer.size / binding.attributeStride;
				if (n == 0)
					continue;

				glm::vec3 const* p = (glm::vec3 const*)(fp);
				glm::vec3 min = *p, max = *p;
				for (int i = 1; i < n; ++i)
				{
					min = glm::min(min, *p);
					max = glm::max(max, *p);
					p = (glm::vec3 const*)(fp + i*binding.attributeStride);
				}

				OBB obb;
				obb.pos = (min + max) / 2.0f;
				obb.half = (max - min) / 2.0f;
				ret->boundingVolume = obb;
			}
		}

		ret->objects = source.objects;

		return ret;
	}

	ShaderLocations::Semantic MakeSemantic(std::string name, unsigned ordinal)
	{
		ShaderLocations::Semantic sem = { name, ordinal };
		return sem;
	}
}