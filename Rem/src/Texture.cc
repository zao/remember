#include "Texture.h"

#include <cassert>
#include "stb_image.h"

GLuint LoadTexture(std::string filename, GLenum componentType)
{
	int w, h, components = 4;
	union
	{
		void* p;
		stbi_uc* i;
		float* f;
	} data;
	
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	
	if (componentType == GL_UNSIGNED_BYTE)
	{
		data.i = stbi_load(filename.c_str(), &w, &h, &components, components);
	}

	assert(data.p);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.i);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0); // We want unlimited mipmaps
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	return tex;
}