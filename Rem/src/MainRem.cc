#include "util/Numeric.h"
#include "util/OpenGL.h"
#include <GL/glfw3.h>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>
#include <boost/regex.hpp>

#include <cassert>
#include <deque>
#include <fstream>
#include <map>
#include <queue>
#include <set>
#include <vector>

#include "util/DebugHelp.h"
#include "util/Resources.h"
#include "TransformAnimation.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"

bool g_running;
int screenWidth = 1280, screenHeight = 720;

void UnloadGameAssets();

int WindowCloseHandler(GLFWwindow wnd)
{
	UnloadGameAssets();
	g_running = false;
	return GL_TRUE;
}

void KeyHandler(GLFWwindow wnd, int key, int pressed)
{
	if (key == GLFW_KEY_ESCAPE)
	{
		UnloadGameAssets();
		g_running = false;
	}
}

struct TextBuffer
{
	size_t width;
	size_t height;

	typedef uint8_t Cell;
	std::vector<Cell> cells;

	TextBuffer(size_t width, size_t height)
		: width(width), height(height)
		, cells(width * height, ' ')
	{}

	void SetLine(int line, std::string text)
	{
		if (text.size() > width)
			text = text.substr(0, width);
		size_t i = 0;
		for (; i < text.size(); ++i)
			cells[line*width + i] = (Cell)text[i];
		for (; i < width; ++i)
			cells[line*width + i] = ' ';
	}
};

struct HUDPanel
{
};

void DrawHUD()
{

}

typedef unsigned TileID;
TileID tileIDSource;

enum { TileRows = 4, TileCols = 5 };

TileID idSupply;

struct BoundingBundle
{
	geom::BoundingVolume local;
	geom::BoundingVolume world;
};

enum TilePhase
{
	Closed,
	Opening,
	Mismatching,
	Matching,
	Open,
	Closing,
};

struct Tiles
{
	TileID ids[TileRows][TileCols];
	std::map<TileID, BoundingBundle> bounds;
	std::map<TileID, glm::mat4> toBoard;
	std::map<TileID, glm::mat4> transforms;
	std::map<TileID, std::pair<int, int>> rowCol;
	std::map<TileID, unsigned> faceValue;

	std::map<TileID, TilePhase> phases;
	std::map<TileID, glm::vec2> flipAxes;
};

Tiles tiles;

struct AnimationState
{
	int startTick;
	boost::shared_ptr<TransformAnimation> anim;
};
std::map<TileID, AnimationState> tileAnimations;

std::vector<glm::mat4> debugTransforms;

GLuint tileProgram = 0;
geom::ShaderLocations tileUniformLocs;
boost::shared_ptr<geom::ResolvedMesh> tileCoord;
std::map<std::string, GLuint> textures;
GLuint clampBilinearSampler;
glm::vec3 boardOrigin = glm::vec3(0.0, 0.0, 0.0);
glm::vec3 tileSpacing = glm::vec3(1.5, 0.0, 1.5);

glm::mat4 boardWorldMatrix;
glm::mat4 viewMatrix;
glm::mat4 projectionMatrix;

int lastMouseX, lastMouseY;
std::deque<glm::vec2> pendingClicks;

enum GamePhase
{
	AwaitingTile,
	OpeningTile,
	ShowingPair,
	RestoreMismatch,
	GameOver,
};

template <GamePhase Phase>
struct PhaseData;

template <>
struct PhaseData<AwaitingTile>
{
	GamePhase phase;
	int tickElapsed;
	bool hasEarlierTile;
	TileID earlierTile;

	static PhaseData NoTile()
	{ PhaseData ret = { AwaitingTile, 0, false, 0 }; return ret; }
	static PhaseData OneTile(TileID tile)
	{ PhaseData ret = { AwaitingTile, 0, true, tile }; return ret; }
};

template <>
struct PhaseData<OpeningTile>
{
	GamePhase phase;
	int tickElapsed;
	bool hasEarlierTile;
	TileID earlierTile;
	TileID tileOpening;
	int tickDuration;

	static PhaseData FirstTile(TileID first, int tickDuration)
	{ PhaseData ret = { OpeningTile, 0, false, 0, first, tickDuration }; return ret; }

	static PhaseData SecondTile(TileID first, TileID second, int tickDuration)
	{ PhaseData ret = { OpeningTile, 0, true, first, second, tickDuration }; return ret; }
};

template <>
struct PhaseData<ShowingPair>
{
	GamePhase phase;
	int tickElapsed;
	TileID firstTile, secondTile;
	int tickDuration;
	bool matched;

	static PhaseData Match(TileID first, TileID second, int tickDuration)
	{ PhaseData ret = { ShowingPair, 0, first, second, tickDuration, true }; return ret; }

	static PhaseData Mismatch(TileID first, TileID second, int tickDuration)
	{ PhaseData ret = { ShowingPair, 0, first, second, tickDuration, false }; return ret; }
};

template <>
struct PhaseData<RestoreMismatch>
{
	GamePhase phase;
	int tickElapsed;
	TileID firstTile, secondTile;
	int tickDuration;

	static PhaseData Make(TileID first, TileID second, int tickDuration)
	{ PhaseData ret = { RestoreMismatch, 0, first, second, tickDuration }; return ret; }
};

template <>
struct PhaseData<GameOver>
{
	GamePhase phase;
	int tickElapsed;

	static PhaseData Make()
	{ PhaseData ret = { GameOver, 0 }; return ret; }
};

struct PhaseCommon
{
	GamePhase phase;
	int tickElapsed;
};

union GamePhaseUnion
{
	PhaseCommon common;
	PhaseData<AwaitingTile> awaitingTile;
	PhaseData<OpeningTile> openingTile;
	PhaseData<ShowingPair> showingPair;
	PhaseData<RestoreMismatch> restoreMismatch;
	PhaseData<GameOver> gameOver;
} gamePhase;

void MouseMoveHandler(GLFWwindow wnd, int x, int y)
{
	lastMouseX = x;
	lastMouseY = y;
}

void MouseButtonHandler(GLFWwindow wnd, int button, int isActive)
{
	if (! isActive)
	{
		pendingClicks.push_back(glm::vec2(lastMouseX, lastMouseY));
	}
}

struct ProgramInterfaceResults
{
	GLint activeResources;
	GLint maxNameLength;
	GLint maxNumActiveVariables;
	GLint maxNumCompatibleSubroutines;
};

ProgramInterfaceResults GetProgramInterface(GLuint program, GLenum iface)
{
	ProgramInterfaceResults res = {};
	glGetProgramInterfaceiv(program, iface, GL_ACTIVE_RESOURCES, &res.activeResources);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NAME_LENGTH, &res.maxNameLength);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NUM_ACTIVE_VARIABLES, &res.maxNumActiveVariables);
	glGetProgramInterfaceiv(program, iface, GL_MAX_NUM_COMPATIBLE_SUBROUTINES, &res.maxNumCompatibleSubroutines);
	return res;
}

void LoadGameAssets()
{
	assert(!tileProgram);
	auto tileVS = ShaderFromResource(GL_VERTEX_SHADER, "tile_vs.glsl");
	auto tileFS = ShaderFromResource(GL_FRAGMENT_SHADER, "tile_fs.glsl");

	tileProgram = glCreateProgram();
	glAttachShader(tileProgram, tileVS.shader);
	glAttachShader(tileProgram, tileFS.shader);
	glLinkProgram(tileProgram);

	GLint status;
	glGetProgramiv(tileProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength = 0;
		glGetProgramiv(tileProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
		std::vector<char> infoLog(infoLogLength);
		glGetProgramInfoLog(tileProgram, infoLog.size(), nullptr, &infoLog[0]);
		OutputDebugStringA(&infoLog[0]);
		OutputDebugStringA("\n");
	}

	glDeleteShader(tileVS.shader);
	glDeleteShader(tileFS.shader);

	{
		geom::ShaderLocations uniformLocs;
	
		char const* names[] = { "model", "view", "projection", "diffuseTexture" };
		char const* semantics[] = { "MODEL", "VIEW", "PROJECTION", "DIFFUSE" };

		for (int i = 0; i < 4; ++i)
		{
			GLuint loc = glGetUniformLocation(tileProgram, names[i]);
			if (loc != (GLuint)-1)
				uniformLocs.mapping[geom::MakeSemantic(semantics[i])] = loc;
		}

		::tileUniformLocs = uniformLocs;
	}

#undef JOSH

	{
		boost::shared_ptr<geom::AbstractMesh> rawTile = geom::LoadObj("res/plain_tile.obj");
#if defined(JOSH)
		rawTile = geom::LoadObj("D:/max_project_files/finalcard_re.obj");
#endif
		geom::ShaderLocations locs;
		geom::ShaderLocations::Semantic sem = {};
		sem.name = "POSITION";
		locs.mapping[sem] = 0;
		sem.name = "NORMAL";
		locs.mapping[sem] = 1;
		sem.name = "TEXCOORD";
		locs.mapping[sem] = 2;

		::tileCoord = geom::ResolveMesh(*rawTile, locs);
	}

	textures["Tile"] = LoadTexture("res/078.png", GL_UNSIGNED_BYTE);
	textures["Image"] = LoadTexture("res/138.jpg", GL_UNSIGNED_BYTE);
	//textures["Tile"] = LoadTexture("res/Fairy_Tail.png", GL_UNSIGNED_BYTE);
#if defined(JOSH)
	textures["03___Default"] = textures["Tile"];
	textures["04___Default"] = textures["Image"];
#endif
	{
		float maxAF = 1.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAF);
		glGenSamplers(1, &clampBilinearSampler);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(clampBilinearSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameterf(clampBilinearSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAF);
	}
}

void UnloadGameAssets()
{
	BOOST_FOREACH(auto const& pair, textures)
	{
		glDeleteTextures(1, &pair.second);
	}
	tileCoord.reset();
	glDeleteProgram(tileProgram);
	tileProgram = 0;
}

template <typename Binding>
void BindStream(GLuint vb, Binding const& binding)
{
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glEnableVertexAttribArray(binding.attributeIndex);
	GLboolean normalized = binding.shouldNormalize ? GL_TRUE : GL_FALSE;
	glVertexAttribPointer(binding.attributeIndex, binding.componentCount,
		binding.componentType, normalized, binding.attributeStride,
		(GLvoid const*)(uintptr_t)binding.byteOffset);
}

template <typename BufferVector, typename BindingSet>
void BindStreams(BufferVector const& vbs, BindingSet const& bindings)
{
	BOOST_FOREACH(auto const& binding, bindings)
	{
		BindStream(*vbs[binding.bufferIndex], binding);
	}
}

template <typename BindingSet>
void UnbindStreams(BindingSet const& bindings)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	std::set<GLuint> arrays;
	BOOST_FOREACH(auto const& binding, bindings)
	{
		arrays.insert(binding.attributeIndex);
	}

	BOOST_FOREACH(auto index, arrays)
	{
		glDisableVertexAttribArray(index);
	}
}

glm::vec4 clickLine[2] = {};

void DrawGame()
{
	double const t = glfwGetTime();
	glUseProgram(tileProgram);
	BindStreams(tileCoord->vbs, tileCoord->bindings);

	geom::ShaderLocations::Semantic semantics[] = {
		geom::MakeSemantic("MODEL"),
		geom::MakeSemantic("VIEW"),
		geom::MakeSemantic("PROJECTION"),
		geom::MakeSemantic("DIFFUSE"),
		geom::MakeSemantic("SELECTED"),
	};

	glUniformMatrix4fv(tileUniformLocs.mapping[semantics[1]], 1, GL_FALSE, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(tileUniformLocs.mapping[semantics[2]], 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	std::deque<std::pair<glm::mat4, boost::shared_ptr<geom::ResolvedMesh>>> drawables;
	BOOST_FOREACH(auto xfp, tiles.transforms)
	{
		drawables.push_back(std::make_pair(xfp.second, tileCoord));
	}

	BOOST_FOREACH(auto xfp, drawables)
	{
		glm::mat4 tileWorld = xfp.first;
		auto entity = xfp.second;
		glUniformMatrix4fv(tileUniformLocs.mapping[semantics[0]], 1, GL_FALSE, glm::value_ptr(tileWorld));
		glUniform1ui(tileUniformLocs.mapping[semantics[3]], 0);
		glBindSampler(0, clampBilinearSampler);

		BOOST_FOREACH(geom::Object obj, entity->objects)
		{
			glActiveTexture(GL_TEXTURE0 + 0);
			glBindTexture(GL_TEXTURE_2D, textures[obj.material]);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *entity->ib);
			glDrawElements(GL_TRIANGLES, obj.primitiveCount*3, GL_UNSIGNED_INT, (GLvoid const*)(obj.offset*4));
		}
	}
	
	UnbindStreams(tileCoord->bindings);
	glUseProgram(0);
}

void ScreenToWorldLineSegment(glm::vec2 point, glm::vec4* nearOut, glm::vec4* farOut)
{
	glm::mat4 screenToNDC =
		glm::translate(-1.0f, 1.0f, 0.0f) *
		glm::scale(2.0f/screenWidth, -2.0f/screenHeight, 1.0f);

	glm::mat4 inv = glm::inverse(projectionMatrix*viewMatrix);
	glm::vec4 screenCoord(point, 0.0, 1.0);
	glm::vec4 ndcCoord = screenToNDC * screenCoord;

	glm::vec4 nearPoint = inv * glm::vec4(ndcCoord.swizzle(glm::X, glm::Y), -1.0f, 1.0f);
	glm::vec4 farPoint  = inv * glm::vec4(ndcCoord.swizzle(glm::X, glm::Y),  1.0f, 1.0f);
		
	nearPoint /= nearPoint.w;
	farPoint /= farPoint.w;
	
	*nearOut = nearPoint;
	*farOut = farPoint;
}

int gameTicks = 0;
double rootTime;

void TickLogic()
{
	glm::vec3 const Y = glm::vec3(0.0, 1.0, 0.0);
	glm::vec3 const Z = glm::vec3(0.0, 0.0, 1.0);

	double const dt = 1.0/120.0;
	double t = glfwGetTime();
	while (t - rootTime > dt * gameTicks)
	{
		gameTicks++;
		projectionMatrix = glm::perspective(90.0f, screenWidth/(float)screenHeight, 1.0f, 1000.0f);
		glm::vec3 eye;
		eye = glm::normalize(glm::vec3(0, 200, 50.0f)) * 4.0f;
		eye = glm::normalize(glm::vec3(0.0f, 50.0f, 10.0f)) * 5.0f;
		viewMatrix =
			glm::lookAt(
			eye,
			glm::vec3(),
			glm::vec3(0, 0, -1))
			* glm::rotate((float)sin(t) * 45.0f, glm::vec3(1.0f, 0.0f, 0.0f));

		gamePhase.common.tickElapsed++;
		switch (gamePhase.common.phase)
		{
			case AwaitingTile:
			{
				auto& ph = gamePhase.awaitingTile;
				if (pendingClicks.size())
				{
					glm::vec4 nearPoint, farPoint;
					ScreenToWorldLineSegment(pendingClicks.front(), &nearPoint, &farPoint);
					pendingClicks.pop_front();

					clickLine[0] = nearPoint;
					clickLine[1] = farPoint;
					
					boost::optional<float> intersectT;
					boost::optional<TileID> intersectID;
					BOOST_FOREACH(auto const& bvp, tiles.bounds)
					{
						auto bv = bvp.second;
						auto res = geom::FindLine(glm::vec3(nearPoint), glm::vec3(farPoint), bv.world);
						if (! res.intersect) continue;

						if (! intersectT || *intersectT > res.t)
						{
							intersectT = res.t;
							intersectID = bvp.first;
							
						}
					}

					bool newMatch = intersectID; // TODO: fixme
					if (newMatch)
					{
						TileID id = *intersectID;

						double animationDuration = 0.3;
						int tickDuration = (int)(animationDuration/dt);

						glm::vec2 axisDirection = tiles.flipAxes[id];
						glm::vec3 rotationAxis(axisDirection.x, 0.0f, axisDirection.y);

						tileAnimations[id].anim.reset(new BounceFlipAnimation((float)animationDuration, 1.0f /*distance*/, Y /*along-axis*/, 0.5f /*rotations*/, rotationAxis /*around-axis*/));
						tileAnimations[id].startTick = gameTicks;

						if (ph.hasEarlierTile)
							gamePhase.openingTile = PhaseData<OpeningTile>::SecondTile(ph.earlierTile, id, tickDuration);
						else
							gamePhase.openingTile = PhaseData<OpeningTile>::FirstTile(id, tickDuration);
					}
				}
				break;
			}
			case OpeningTile:
			{
				auto& ph = gamePhase.openingTile;

				pendingClicks.clear();
				
				if (ph.tickElapsed >= ph.tickDuration)
				{
					if (ph.hasEarlierTile)
						if (tiles.faceValue[ph.earlierTile] == tiles.faceValue[ph.tileOpening])
						{
							double duration = 0.0;
#if 0
							tileAnimations[ph.earlierTile].anim = tileAnimations[ph.tileOpening].anim =
								boost::make_shared<BounceFlipAnimation>(duration, 0.0, Y, 1.0f, Y, 180.0f);
							tileAnimations[ph.earlierTile].startTick = tileAnimations[ph.tileOpening].startTick = gameTicks;
#endif
							gamePhase.showingPair = PhaseData<ShowingPair>::Match(ph.earlierTile, ph.tileOpening, (int)(duration/dt));
						}
						else
						{
							gamePhase.showingPair = PhaseData<ShowingPair>::Mismatch(ph.earlierTile, ph.tileOpening, (int)(1.0/dt));
						}
					else
						gamePhase.awaitingTile = PhaseData<AwaitingTile>::OneTile(ph.tileOpening);
				}
				break;
			}
			case ShowingPair:
			{
				auto& ph = gamePhase.showingPair;
				auto& tileA = tiles.ids[ph.firstTile];
				auto& tileB = tiles.ids[ph.secondTile];
				// if first tick: check matching

				if (ph.tickElapsed >= ph.tickDuration)
				{
					if (ph.matched)
					{
						gamePhase.awaitingTile = PhaseData<AwaitingTile>::NoTile();
					}
					else
					{
						double duration = 2.5;
						gamePhase.restoreMismatch = PhaseData<RestoreMismatch>::Make(ph.firstTile, ph.secondTile, (int)(duration/dt));
					}
				}
				break;
			}
			case RestoreMismatch:
			{
				auto& ph = gamePhase.restoreMismatch;

				if (ph.tickElapsed >= ph.tickDuration)
				{
					gamePhase.awaitingTile = PhaseData<AwaitingTile>::NoTile();
				}

				break;
			}
			case GameOver:
			{
				break;
			}
		}

		boardWorldMatrix = glm::translate(boardOrigin);

		std::map<TileID, glm::mat4> localTransforms;
		BOOST_FOREACH(auto& a, tileAnimations)
		{
			float t = (float)(dt * (gameTicks - a.second.startTick));
			localTransforms[a.first] = a.second.anim->Compute(t);
		}

		for (auto I = tiles.toBoard.begin(); I != tiles.toBoard.end(); ++I)
		{
			auto id = I->first;
			glm::mat4 const& shiftMatrix = I->second;
			auto J = localTransforms.find(id);
			glm::mat4 local;
			if (J != localTransforms.end())
				local = J->second;
			glm::mat4 xform = tiles.transforms[id] = boardWorldMatrix * shiftMatrix * local;
			tiles.bounds[id].world = geom::TransformBoundingVolume(xform, tiles.bounds[id].local);
		}
	}
}

void UpdateVisuals()
{
}

int Main()
{
	glfwInit();
	{
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		glfwWindowHint(GLFW_FSAA_SAMPLES, 4);
		GLFWwindow wnd = glfwCreateWindow(screenWidth, screenHeight, GLFW_WINDOWED, "Remember", nullptr);
		glfwSetWindowCloseCallback(wnd, &WindowCloseHandler);
		glfwSetKeyCallback(wnd, &KeyHandler);
		glfwSetMouseButtonCallback(wnd, &MouseButtonHandler);
		glfwSetCursorPosCallback(wnd, &MouseMoveHandler);
		glfwMakeContextCurrent(wnd);
		gl3wInit();

		LoadGameAssets();
		tileIDSource = 0;

		for (int row = 0; row < TileRows; ++row)
		{
			for (int col = 0; col < TileCols; ++col)
			{
				auto id = tileIDSource++;
				tiles.ids[row][col] = id;
				tiles.bounds[id].local = tileCoord->boundingVolume;
				tiles.faceValue[id] = 0;
				tiles.flipAxes[id] = math::RandomDirection2D();
				tiles.phases[id] = Closed;
				tiles.rowCol[id] = std::make_pair(row, col);
				tiles.toBoard[id] = glm::translate(glm::vec3(col - (TileCols-1)/2.0f, 0.0f, row - (TileRows-1)/2.0f) * tileSpacing);
				tiles.transforms[id] = glm::mat4();
			}
		}

		gamePhase.awaitingTile = PhaseData<AwaitingTile>::NoTile();
		
		glEnable(GL_DEPTH_TEST);

		g_running = true;
		glClearColor(0.1f, 0.3f, 0.2f, 1.0f);
		glClearDepth(1.0);
		while (g_running)
		{
			TickLogic();
			UpdateVisuals();
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			DrawGame();
			DrawHUD();
			glfwPollEvents();
			glfwSwapBuffers(wnd);
		}
		glfwDestroyWindow(wnd);
	}
	glfwTerminate();
	return 0;
}

#if defined(WIN32)
#include <Windows.h>
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	return Main();
}
#else
int main()
{
	return Main();
}
#endif
