#pragma once
#include "util/OpenGL.h"
#include <string>

struct AnnotatedShader
{
	GLuint shader;
};

AnnotatedShader ShaderFromResource(GLenum type, std::string name);