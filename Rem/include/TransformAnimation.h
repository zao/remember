#pragma once
#include "util/Numeric.h"

class TransformAnimation
{
public:
	virtual ~TransformAnimation() {}
	virtual glm::mat4 Compute(double t) const = 0;
};

class BounceFlipAnimation : public TransformAnimation
{
public:
	BounceFlipAnimation(float duration, float distance, glm::vec3 alongAxis, float rotations, glm::vec3 aroundAxis, float angleOffset = 0.0f);
	
	virtual glm::mat4 Compute(double t) const override;

private:
	float duration;
	float distance;
	glm::vec3 alongAxis;
	float rotations;
	glm::vec3 aroundAxis;
	float angleOffset;
};
