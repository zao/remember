#pragma once

#if !defined(WIN32)
#include <iostream>

inline void OutputDebugStringA(char const* str)
{
	std::cerr << str << std::flush;
}
#endif

