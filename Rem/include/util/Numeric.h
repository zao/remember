#pragma once

#include <cstdint>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace math
{
	inline float Random01()
	{
		auto r = rand();
		return r/(float)(RAND_MAX);
	}

	inline float Pi()
	{
		return glm::radians(180.0f);
	}

	inline glm::vec2 RandomDirection2D()
	{
	  float azimuth = Random01() * 2 * Pi();
	  return glm::vec2(cos(azimuth), sin(azimuth));
	}

	inline glm::vec3 RandomDirection3D()
	{
	  float z = (2*Random01()) - 1; // z is in the range [-1,1]
	  glm::vec2 planar = RandomDirection2D() * sqrtf(1-z*z);
	  return glm::vec3(planar.x, planar.y, z);
	}
}
