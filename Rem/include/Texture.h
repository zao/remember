#pragma once
#include "util/OpenGL.h"
#include <string>

GLuint LoadTexture(std::string filename, GLenum componentType);