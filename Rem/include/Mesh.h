#pragma once

#include "util/OpenGL.h"
#include "util/Numeric.h"
#include "util/SmartPointer.h"

#include <map>
#include <set>
#include <string>
#include <vector>

#include <boost/variant.hpp>

namespace geom
{
	struct OBB
	{
		glm::vec3 pos;
		glm::mat3 basis;
		glm::vec3 half;
	};

	struct AABB
	{
		glm::vec3 min;
		glm::vec3 max;
	};

	typedef boost::variant<OBB, AABB> BoundingVolume;

	bool TestLineAABB(glm::vec3 p0, glm::vec3 p1, AABB const& b);
	bool TestLineOBB(glm::vec3 p0, glm::vec3 p1, OBB const& b);

	struct LineFindResult
	{
		bool intersect;
		float t;

		static LineFindResult Reject();
		static LineFindResult Intersect(float t);
	};

	LineFindResult FindLineAABB(glm::vec3 p0, glm::vec3 p1, AABB const& b);
	LineFindResult FindLineOBB(glm::vec3 p0, glm::vec3 p1, OBB const& b);
	LineFindResult FindLine(glm::vec3 p0, glm::vec3 p1, BoundingVolume const& bv);

	BoundingVolume TransformBoundingVolume(glm::mat4 const& xform, BoundingVolume const& bv);


	struct CommonBinding
	{
		unsigned bufferIndex;
		unsigned componentCount;
		GLenum componentType;
		GLboolean shouldNormalize;
		GLsizei attributeStride;
		unsigned byteOffset;
	};

	struct AbstractBinding : CommonBinding
	{
		std::string semanticName;
		unsigned semanticOrdinal;
	};

	AbstractBinding MakeAbstractBinding(
		std::string semanticName,
		unsigned semanticOrdinal,
		unsigned bufferIndex,
		unsigned componentCount,
		GLenum componentType,
		GLboolean shouldNormalize,
		GLsizei attributeStride,
		unsigned byteOffset);

	struct ResolvedBinding : CommonBinding
	{
		unsigned attributeIndex;
	};

	struct CompareSemantics
	{
		bool operator () (AbstractBinding const& a, AbstractBinding const& b) const
		{
			if (a.semanticName != b.semanticName)
				return a.semanticName < b.semanticName;
			return a.semanticOrdinal < b.semanticOrdinal;
		}

		bool operator () (ResolvedBinding const& a, ResolvedBinding const& b) const
		{
			return a.attributeIndex < b.attributeIndex;
		}
	};

	struct RawBuffer
	{
		boost::shared_ptr<char> data;
		size_t size;
	};

	template <typename Vector>
	RawBuffer MakeRawBuffer(Vector const& v)
	{
		RawBuffer b;
		b.size = sizeof(typename Vector::value_type)*v.size();
		b.data.reset(new char[b.size], [](char* p) { delete [] p; });
		memcpy(b.data.get(), &v[0], b.size);
		return b;
	}

	struct Object
	{
		std::string name;
		std::string material;
		size_t offset;
		size_t primitiveCount;
	};

	struct AbstractMesh
	{
		std::set<AbstractBinding, CompareSemantics> bindings;
		std::vector<RawBuffer> vertexBuffers;
		RawBuffer indexBuffer;
		std::vector<Object> objects;
	};

	struct ResolvedMesh
	{
		std::set<ResolvedBinding, CompareSemantics> bindings;
		std::vector<boost::shared_ptr<GLuint>> vbs;
		boost::shared_ptr<GLuint> ib;
		std::vector<Object> objects;
		BoundingVolume boundingVolume;
	};

	struct ShaderLocations
	{
		struct Semantic
		{
			std::string name;
			unsigned ordinal;
			bool operator < (Semantic const& rhs) const
			{
				if (name != rhs.name) return name < rhs.name;
				return ordinal < rhs.ordinal;
			}
		};
		std::map<Semantic, unsigned> mapping;
	};

	ShaderLocations::Semantic MakeSemantic(std::string name, unsigned ordinal = 0);

	boost::shared_ptr<AbstractMesh> LoadObj(std::string filename);
	boost::shared_ptr<AbstractMesh> LoadMsh(std::string filename);
	OBB ComputeOBB(AbstractMesh const& source);
	boost::shared_ptr<ResolvedMesh> ResolveMesh(AbstractMesh const& source, ShaderLocations const& locs);
}