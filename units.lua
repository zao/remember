-- StaticLibrary {
-- 	Name = "WeeSoo",
-- 	Sources = {
-- 		"WeeSoo/include/WeeSoo.h",
-- 		"WeeSoo/src/WeeSooPool.cc",
-- 	},
-- 	Env = {
-- 		CPPPATH = {
-- 			"WeeSoo/include",
-- 		},
-- 	},
-- }
-- 
-- StaticLibrary {
-- 	Name = "DeeDee",
-- 	Sources = {
-- 		"DeeDee/include/DeeDee.h",
-- 		"DeeDee/src/DeeDee.cc",
-- 		"DeeDee/src/stb_image.c",
-- 	},
-- 	Env = {
-- 		CPPPATH = {
-- 			"DeeDee/include",
-- 		},
-- 	},
-- }

StaticLibrary {
	Name = "gl3w",
	Sources = {
		"dep/gl3w/src/gl3w.c",
		"dep/gl3w/include/GL/gl3w.h",
		"dep/gl3w/include/GL/glcorearb.h",
	},
}

StaticLibrary {
	Name = "msgpack-c",
	SourceDir = "dep/msgpack-c/src/",
	Sources = {
		"objectc.cpp",
		"unpack.cpp",
		"version.cpp",
		"vrefbuffer.cpp",
		"zone.cpp",
		"gcc_atomic.cpp",
		"object.cpp",
	},
	Propagate = {
		Env = { CPPPATH = "dep/msgpack-c/src", },
	},
	Env = { CPPPATH = "dep/msgpack-c/src", },
}

StaticLibrary {
	Name = "stb_image",
	SourceDir = "dep/stb_image/src/",
	Sources = {
		"stb_image.c",
	},
	Propagate = {
		Env = { CPPPATH = "dep/stb_image/include", },
	},
	Env = { CPPPATH = "dep/stb_image/include", },
}

Program {
	Name = "Rem",
	Sources = {
		"Rem.rc",
		"Rem/src/MainRem.cc",
		"Rem/src/Mesh.cc",
		"Rem/src/Mesh.Obj.cc",
		"Rem/src/Shader.cc",
		"Rem/src/Texture.cc",
		"Rem/src/TransformAnimation.cc",
		"Rem/src/util/OpenGL.cc",
	},
	Env = {
		CPPPATH = {
			"Rem/include",
			"Rem/include/util",
		},
	},
	Libs = {
		{
			"opengl32.lib", "gdi32.lib", "user32.lib"; Config = "win*-vc10",
			{ "glfw3-vc100-gd.lib"; Config = "*-*-debug" },
			{ "glfw3-vc100.lib"; Config = "*-*-release" },
			{ "glfw3-vc100.lib"; Config = "*-*-production" },
		},
	},
	Depends = { "gl3w", "msgpack-c", "stb_image" },
}

Default "Rem"

-- vim: ts=4 sw=4 noet cin
